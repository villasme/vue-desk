const path = require('path')
// const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const resolve = apath => path.resolve(__dirname, '..', ...apath)

const config = {
  mode: 'development',
  entry: ['./src/main.js'],
  output: {
    filename: '[name].js',
    path: resolve(['dist']),
    publicPath: '/'
  },
  devtool: 'cheap-module-eval-source-map',
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      // this will apply to both plain `.js` files
      // AND `<script>` blocks in `.vue` files
      // {
      //   enforce: "pre",
      //   test: /\.js$/,
      //   use: ['eslint-loader'],
      //   exclude: /node_modules/
      // },
      {
        test: /\.js$/,
        use: ['babel-loader', 'eslint-loader'],
        exclude: /node_modules/
      },
      // this will apply to both plain `.css` files
      // AND `<style>` blocks in `.vue` files
      {
        test: /\.css$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: 'vueDesk-[path]--[local]_[hash:base64:8]'
            }
          },
          'postcss-loader'
        ]
      },
      {
        test: /\.scss$/,
        use: [
          'css-hot-loader',
          { loader: MiniCssExtractPlugin.loader },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: 'vueDesk-[path]--[local]_[hash:base64:8]'
            }
          },
          'postcss-loader',
          {
            loader: 'px2rem-loader',
            options: {
              remUnit: 32,
              remPrecision: 8
            }
          },
          'sass-loader'
        ]
      },
      {
        test: /\.less$/,
        use: [
          { loader: MiniCssExtractPlugin.loader },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: 'vueDesk-[path]--[local]_[hash:base64:8]'
            }
          },
          'postcss-loader',
          {
            loader: 'px2rem-loader',
            options: {
              remUnit: 32,
              remPrecision: 8
            }
          },
          'less-loader'
        ]
      }
    ]
  },
  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js' // 用 webpack 1 时需用 'vue/dist/vue.common.js'
    }
  },
  plugins: [
    // new webpack.HotModuleReplacementPlugin(),
    // new webpack.NamedModulesPlugin(), // HMR shows correct file names in console on update.
    // new webpack.NoEmitOnErrorsPlugin(),
    // // make sure to include the plugin!
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '..', 'src/index.html')
    }),
    new MiniCssExtractPlugin({
      filename: '[name].my.css',
      chunkFilename: '[id].css'
    })
  ]
}

module.exports = config
