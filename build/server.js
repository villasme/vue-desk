const Koa = require('koa')
// const sockets = require('./sockets')
const webpack = require('webpack')
const config = require('../config')
const webpackConfig = require('./webpack.config')
// const { devMiddleware, hotMiddleware } = require('koa-webpack-middleware')
const koaWebpack = require('koa-webpack')

const app = new Koa()

const compiler = webpack(webpackConfig)

koaWebpack({
  compiler
}).then(middleware => {
  app.use(middleware)
})
// app.use(
//   devMiddleware(compiler, {
//     publicPath: config.output.publicPath,
//     stats: { colors: true },
//     quiet: false,
//     headers: { 'X-Custom-Header': 'yes', nihaosdjfl: 'hahahah' }
//   })
// )

// app.use(
//   hotMiddleware(compiler, {
//     heartbeat: 10 * 1000
//   })
// )

app.listen(config.dev.port)
// sockets(app)
console.log('listent-----' + config.dev.port)
