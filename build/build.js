const webpack = require('webpack')
const webConfig = require('./webpack.config')

const callback = function (err, stats) {
  console.log(err)
  console.log(stats)
}

webpack(webConfig).run(callback)
