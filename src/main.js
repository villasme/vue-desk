import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import route from './router'

Vue.config.productionTip = false

Vue.use(VueRouter)
const router = route(VueRouter)

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
