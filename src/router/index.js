import A from '../A/index.vue'

const routes = [{ path: '/a', component: A }]

export default VueRouter => {
  const router = new VueRouter({
    routes
  })
  return router
}
